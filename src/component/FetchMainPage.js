import React, { Component } from 'react';
import DisplayAllCountries from './DisplayAllCountries';
import "./styles.css"
import "bootstrap/dist/css/bootstrap.min.css"
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Modal from './modal';




// js code for loader 

var myVar;
myFunction()
function myFunction() {
    myVar = setTimeout(showPage, 600);
}

function showPage() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("myDiv").style.display = "block";
}



class FetchMainPage extends Component {
    
    constructor() {
        super();
        this.state = {
            countriesData: [],
            clickedRegion: 0,
            continent: 0,
            popup: ""
        };
    }

    componentDidMount() {
        this.fetchData()

    }
    fetchData = () => {
        fetch("https://restcountries.com/v3.1/all")
            .then((data) => data.json())
            .then((data) => {
                let count = 0;
                return data.reduce((accumulator, currentvalue) => {
                    let flag = currentvalue.flags.png;
                    let country = currentvalue.name.common;
                    let population = currentvalue.population;
                    let region = currentvalue.region;
                    let capital = currentvalue.capital;
                    let subregion = currentvalue.subregion;
                    let languages = currentvalue.languages;
                    let currencies = currentvalue.currencies;
                    let borders = currentvalue.borders
                    if (borders == undefined) {
                        borders = "nil"
                    }

                    accumulator[count] = {};
                    accumulator[count]["flag"] = flag;
                    accumulator[count]["country"] = country;
                    accumulator[count]["population"] = population;
                    accumulator[count]["region"] = region;
                    accumulator[count]["capital"] = capital;
                    accumulator[count]["region"] = region;
                    accumulator[count]["subregion"] = subregion;
                    accumulator[count]["languages"] = languages;
                    accumulator[count]["currencies"] = currencies;
                    accumulator[count]["borders"] = borders;

                    count++;
                    return accumulator;
                }, []);
            }).then((data) => {
                this.setState({
                    countriesData: data
                })

            }).catch((err) => {
                console.log(err)
            })


    }



    selectedCountry = (name) => {
        let countryName = name.a
        let currentData = this.state.countriesData
        currentData.map((ele) => {
            if (ele.country == countryName) {
                console.log(ele)
                let countryDetail = ["country:", ele.country, "capital:", ele.capital, "popolutaion:", ele.population, "region:", ele.region, "subregion :", ele.subregion, ele.flag]
                alert(countryDetail)
                //return <Modal country={ele.country} capital={ele.capital}></Modal>          
                
            }
        })

    }

    regions = (continent) => {
        console.log(continent)
        this.setState({
            clickedRegion: 1,
            continent: continent
        })

    }
    /*
    changedata = (newdata) => {
        if (this.state.clickedRegion == 1) {
            this.setState({
                countriesData: newdata
            })

        }


    }*/
    render() {
        let countriesArr = []
        let newdata = this.state.countriesData
        if ((this.state.clickedRegion == true) & (this.state.continent != "All regions")) {
            let updatedData = this.state.countriesData.filter((ele) => {
                if (ele.region == this.state.continent) {
                    return ele
                }
            })
            updatedData.map((element) => {
                countriesArr.push(<DisplayAllCountries
                    key={element.country}
                    name={element.country}
                    capital={element.capital}
                    region={element.region}
                    subregion={element.subregion}
                    population={element.population}
                    borders={element.borders}
                    flag={element.flag}
                    method={this.selectedCountry}

                ></DisplayAllCountries>)
                //console.log(element)
            })

        }

        else {
            this.state.countriesData.map((element) => {
                countriesArr.push(<DisplayAllCountries
                    key={element.country}
                    name={element.country}
                    capital={element.capital}
                    region={element.region}
                    subregion={element.subregion}
                    population={element.population}
                    borders={element.borders}
                    flag={element.flag}
                    method={this.selectedCountry}

                ></DisplayAllCountries>)
                //console.log(element)
            })
        }
        return (
            <div>
                <header className='headerClass'>
                    <h1 className='heading'>Where in the world !</h1>
                    <DropdownButton className='dropdownClass' id="dropdown-basic-button" title="Select region">
                        <Dropdown.Item onClick={() => this.regions("All regions")}>All regions</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.regions("Asia")} >Asia</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.regions("Africa")} >Africa</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.regions("Americas")}>Americas</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.regions("Europe")}>Europe</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.regions("Oceania")}>Oceania</Dropdown.Item>
                    </DropdownButton>

                </header>
            


                <div className='countriesContainer'>
                    <div>{countriesArr}</div>


                </div>
               

            </div>
        );
    }
}

export default FetchMainPage;
