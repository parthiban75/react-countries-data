import React, { Component } from 'react'
import "./styles.css"


class DisplayAllCountries extends Component {
    
    popupdisplay = (name) => {
        this.props.method(name)
    }
  
    render() {
        console.log(this.props)
        let a= this.props.name

        return (
                < div className='countryDetails' onClick={()=>this.popupdisplay({a})} >
                    <img className='textclass' src={this.props.flag} alt="" ></img>
                    <p className='textclass'>country name: {this.props.name}</p>
                    <p className='textclass'>country population: {this.props.population}</p>
                    <p className='textclass'>country capital: {this.props.capital}</p>
                    <p className='textclass'>country region: {this.props.region}</p>
                </div >
        )
    }
}

export default DisplayAllCountries